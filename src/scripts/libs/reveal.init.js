import Reveal from 'reveal.js'
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);

import ConfettiGenerator from "confetti-js";
var confettiSettings = { target: 'confetti' };
var confetti = new ConfettiGenerator(confettiSettings);

import { Fireworks } from 'fireworks-js'

export function initReveal() {
    let presentation = new Reveal()
    presentation.initialize().then(() => {
        
        confetti.render();

        var ctx = document.getElementById('myChart');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['shy',
                    'creative',
                    'impulsive',
                    'with a slightly shady sense of humor',
                    'and a little bit of evil'],
                datasets: [{
                    data: [10, 60, 5, 20, 5],
                    backgroundColor: [
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 206, 86)',
                        'rgb(75, 192, 192)',
                        'rgb(255, 170, 86)',
                    ]
                }]
            },
            options: {
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                plugins: {
                    legend: {
                        labels: {
                            // This more specific font property overrides the global property
                            font: {
                                size: 18
                            }
                        }
                    }
                }
            }
        });
    })

    presentation.addEventListener('fireworks', function(){
        const container = document.querySelector('.fireworksContainer')
        const fireworks = new Fireworks(container)
        confetti.clear()
        fireworks.start()
    })

}